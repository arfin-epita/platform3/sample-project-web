

export default
{
  methods:
  {
    fromNowFormatter(val)
    {
      return this.$moment(val).fromNow();
    },

    amountFormatter(val)
    {
      let retStr = "";

      if (val)
        retStr = `$ ${(this.$options.filters.numeralFormat(parseInt(val)))}`;
      else
        retStr = "0";

      return retStr;
    },
  }
}